from random import randint
import sys


def search(arr, param):
    left = 0
    right = len(arr) - 1
    while left <= right:
        middle = (left + right) // 2
        if arr[middle] == param:
            return middle
        elif arr[middle] < param:
            left = middle + 1
        else:
            right = middle - 1
    return -1


mas = []
for ind in range(100):
    mas.append(randint(1, 200))
mas = sorted(mas)
param = int(sys.argv[1])
print(mas, "\n", search(mas, param))
