import sys
from random import randint
import redis
from flask import Flask, request, render_template

red = redis.Redis(host='localhost', port=6379, db=0)

app = Flask(__name__, template_folder='/home/lina/builds/mdztzZC8w/0/Lina-WF/automation/task_4/temp/')

def search(arr, param):
    left = 0
    right = len(arr) - 1
    while left <= right:
        middle = (left + right) // 2
        if arr[middle] == param:
            return middle
        elif arr[middle] < param:
            left = middle + 1
        else:
            right = middle - 1
    return -1


@app.route('/task4_input.html')
def inp():
    return render_template('task4_input.html')


@app.route('/task4_output.html', methods=['GET'])
def outp():
    mas = []
    if red.exists('my_data'):
        mas = [int(num) for num in red.lrange('my_data', 0, -1)]
        action = "Массив выгружен из базы данных:"
    else:
        for ind in range(100):
            mas.append(randint(1, 200))
            mas = sorted(mas)
        red.rpush('my_data', *mas)
        action = "Массив загружен в базу данных:"
    if request.args.get('number'):
        param = int(request.args.get('number'))
    else:
        param = int(sys.argv[1])
        action = "Вы ничего не ввели. В качестве искомого используется " + param + ". " + action
    found_ind = search(mas, param)
    mas_ind = [ind for ind in range(100)]
    return render_template('task4_output.html', action=action, mas=mas, mas_ind=mas_ind, found_ind=found_ind)


if __name__ == '__main__':
    app.run(port=8080)
